const k =(reduced , current )=> reduced+=current  ;

function reduce(elements, cb, startingValue) {

    let index = startingValue;
    let current = elements[index];
    let reduced = current;

    for (let i = index + 1; i < elements.length; i++) {
        reduced = k(reduced, elements[i])
    }
    return reduced

}
console.log();

exports.k = k;

exports.reduce = reduce;