
module.exports= function each(elements, cb) {
    
    let final=[];

    for(let value of elements){
        final.push(cb(value))
    }
    return final ;
}